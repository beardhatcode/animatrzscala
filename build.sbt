name := "animatrz-domain"

version := "0.001"

scalaVersion := "2.11.6"

libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % Test

libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1200-jdbc41"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
