package be.animatrz.domain.dao;

import be.animatrz.domain.data.QuizQuestion;
import be.animatrz.domain.marker.Category;
import junit.framework.TestCase;

/**
 * Created by garonn on 13/06/15.
 */
public class CategoryDAOTest extends TestCase {

    private final CategoryDAO<QuizQuestion> dao = CategoryDAO$.MODULE$.forQuizQuestion();

    public void testGetData() throws Exception {
        Category<QuizQuestion> a = dao.byShortName("algemeen").get();
        System.out.println(a.name());
        System.out.println(a.description());
        System.out.println(a.shortName());
        System.out.println(a);
    }
}
