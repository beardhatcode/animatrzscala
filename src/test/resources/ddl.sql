drop schema public cascade;
create schema public;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: data_tags; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE data_tags (
    id integer NOT NULL,
    name text NOT NULL,
    same integer
);


ALTER TABLE data_tags OWNER TO animatrz;

--
-- Name: quiz_questions_category; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE quiz_questions_category (
    id integer NOT NULL,
    parent integer,
    name text NOT NULL,
    short_name character varying(50),
    icon character varying(50),
    description text
);


ALTER TABLE quiz_questions_category OWNER TO animatrz;

--
-- Name: quiz_questions_category-question; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE "quiz_questions_category-question" (
    question integer NOT NULL,
    category integer NOT NULL
);


ALTER TABLE "quiz_questions_category-question" OWNER TO animatrz;

--
-- Name: quiz_questions_category_id_seq; Type: SEQUENCE; Schema: public; Owner: animatrz
--

CREATE SEQUENCE quiz_questions_category_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE quiz_questions_category_id_seq OWNER TO animatrz;

--
-- Name: quiz_questions_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: animatrz
--

ALTER SEQUENCE quiz_questions_category_id_seq OWNED BY quiz_questions_category.id;


--
-- Name: quiz_questions_question; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE quiz_questions_question (
    id integer NOT NULL,
    question text NOT NULL,
    answer text NOT NULL,
    extra text
);


ALTER TABLE quiz_questions_question OWNER TO animatrz;

--
-- Name: quiz_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: animatrz
--

CREATE SEQUENCE quiz_questions_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE quiz_questions_id_seq OWNER TO animatrz;

--
-- Name: quiz_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: animatrz
--

ALTER SEQUENCE quiz_questions_id_seq OWNED BY quiz_questions_question.id;


--
-- Name: quiz_questions_tags-question; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE "quiz_questions_tags-question" (
    question integer NOT NULL,
    tag integer NOT NULL
);


ALTER TABLE "quiz_questions_tags-question" OWNER TO animatrz;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: animatrz
--

CREATE SEQUENCE tags_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE tags_id_seq OWNER TO animatrz;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: animatrz
--

ALTER SEQUENCE tags_id_seq OWNED BY data_tags.id;


--
-- Name: tasks_category; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE tasks_category (
    id integer NOT NULL,
    parent integer,
    name character varying NOT NULL,
    short_name character varying(50),
    icon character varying(50),
    description character varying
);


ALTER TABLE tasks_category OWNER TO animatrz;

--
-- Name: task_category_id_seq; Type: SEQUENCE; Schema: public; Owner: animatrz
--

CREATE SEQUENCE task_category_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE task_category_id_seq OWNER TO animatrz;

--
-- Name: task_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: animatrz
--

ALTER SEQUENCE task_category_id_seq OWNED BY tasks_category.id;


--
-- Name: tasks_task; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE tasks_task (
    id integer NOT NULL,
    what text NOT NULL,
    extra text
);


ALTER TABLE tasks_task OWNER TO animatrz;

--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: animatrz
--

CREATE SEQUENCE task_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE task_id_seq OWNER TO animatrz;

--
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: animatrz
--

ALTER SEQUENCE task_id_seq OWNED BY tasks_task.id;


--
-- Name: tasks_category-task; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE "tasks_category-task" (
    task integer NOT NULL,
    category integer NOT NULL
);


ALTER TABLE "tasks_category-task" OWNER TO animatrz;

--
-- Name: tasks_tags-task; Type: TABLE; Schema: public; Owner: animatrz; Tablespace:
--

CREATE TABLE "tasks_tags-task" (
    task integer NOT NULL,
    tag integer NOT NULL
);


ALTER TABLE "tasks_tags-task" OWNER TO animatrz;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY data_tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY quiz_questions_category ALTER COLUMN id SET DEFAULT nextval('quiz_questions_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY quiz_questions_question ALTER COLUMN id SET DEFAULT nextval('quiz_questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY tasks_category ALTER COLUMN id SET DEFAULT nextval('task_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY tasks_task ALTER COLUMN id SET DEFAULT nextval('task_id_seq'::regclass);


--
-- Data for Name: data_tags; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY data_tags (id, name, same) FROM stdin;
1	Lieselotte	\N
2	Schoonheid	1
3	TESTTAG	\N
\.


--
-- Data for Name: quiz_questions_category; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY quiz_questions_category (id, parent, name, short_name, icon, description) FROM stdin;
2	\N	Type	\N	\N	\N
1	2	Algemeen	algemeen	\N	Algemene kennis
3	\N	Soort antwoord	\N	\N	\N
5	2	Boeken en stuff	literatuur	\N	Vragen over boeken, strips en meer
4	3	Ja of nee	ja-nee	\N	Vragen die enkel met ja of neen te benatwoorden zijn
6	3	Multiple choise	mult-choise	\N	ferfzerf
\.


--
-- Data for Name: quiz_questions_category-question; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY "quiz_questions_category-question" (question, category) FROM stdin;
1	1
2	1
1	6
\.


--
-- Name: quiz_questions_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: animatrz
--

SELECT pg_catalog.setval('quiz_questions_category_id_seq', 6, true);


--
-- Name: quiz_questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: animatrz
--

SELECT pg_catalog.setval('quiz_questions_id_seq', 2, true);


--
-- Data for Name: quiz_questions_question; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY quiz_questions_question (id, question, answer, extra) FROM stdin;
1	Wie is het mooiste meisje ooit?	Lieselotte	Jaja de enige echte...
2	Is de maan een cirkel	nee	Het is een bol
\.


--
-- Data for Name: quiz_questions_tags-question; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY "quiz_questions_tags-question" (question, tag) FROM stdin;
1	1
2	2
1	2
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: animatrz
--

SELECT pg_catalog.setval('tags_id_seq', 4, true);


--
-- Name: task_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: animatrz
--

SELECT pg_catalog.setval('task_category_id_seq', 7, true);


--
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: animatrz
--

SELECT pg_catalog.setval('task_id_seq', 2, true);


--
-- Data for Name: tasks_category; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY tasks_category (id, parent, name, short_name, icon, description) FROM stdin;
3	\N	Aantal spelers	\N	\N	\N
1	3	Per 2	duo	\N	ola
2	3	Alleen	solo	\N	boter
4	\N	Plaats	\N	\N	\N
5	4	Buiten	buiten	\N	Voor buiten
6	4	Binnen	binnen	\N	voor binnen
7	4	TEST	TEST	\N	Test
\.


--
-- Data for Name: tasks_category-task; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY "tasks_category-task" (task, category) FROM stdin;
1	2
2	2
1	6
\.


--
-- Data for Name: tasks_tags-task; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY "tasks_tags-task" (task, tag) FROM stdin;
1	1
2	2
1	2
\.


--
-- Data for Name: tasks_task; Type: TABLE DATA; Schema: public; Owner: animatrz
--

COPY tasks_task (id, what, extra) FROM stdin;
1	Doe een dansje	een mooi dansje
2	tel het aantal bomen op het plein	Als je ze zelfs niet hebt geteld, laat 3 mensen het tegelijk doen en vergelijk de antwoorden
\.


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY quiz_questions_category
ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: quiz_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY quiz_questions_question
ADD CONSTRAINT quiz_questions_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY data_tags
ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: task_category_pkey; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY tasks_category
ADD CONSTRAINT task_category_pkey PRIMARY KEY (id);


--
-- Name: task_pkey; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY tasks_task
ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: unique_name; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY data_tags
ADD CONSTRAINT unique_name UNIQUE (name);


--
-- Name: unique_short_name; Type: CONSTRAINT; Schema: public; Owner: animatrz; Tablespace:
--

ALTER TABLE ONLY quiz_questions_category
ADD CONSTRAINT unique_short_name UNIQUE (short_name);


--
-- Name: category_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY quiz_questions_category
ADD CONSTRAINT category_parent_fkey FOREIGN KEY (parent) REFERENCES quiz_questions_category(id);


--
-- Name: quiz_question_category-data_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "quiz_questions_category-question"
ADD CONSTRAINT "quiz_question_category-data_category_fkey" FOREIGN KEY (category) REFERENCES quiz_questions_category(id);


--
-- Name: quiz_question_category-data_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "quiz_questions_category-question"
ADD CONSTRAINT "quiz_question_category-data_question_fkey" FOREIGN KEY (question) REFERENCES quiz_questions_question(id);


--
-- Name: quiz_questions_tags-data_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "quiz_questions_tags-question"
ADD CONSTRAINT "quiz_questions_tags-data_question_fkey" FOREIGN KEY (question) REFERENCES quiz_questions_question(id);


--
-- Name: quiz_questions_tags-data_tag_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "quiz_questions_tags-question"
ADD CONSTRAINT "quiz_questions_tags-data_tag_fkey" FOREIGN KEY (tag) REFERENCES data_tags(id);


--
-- Name: tags_same_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY data_tags
ADD CONSTRAINT tags_same_fkey FOREIGN KEY (same) REFERENCES data_tags(id);


--
-- Name: task_category_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY tasks_category
ADD CONSTRAINT task_category_parent_fkey FOREIGN KEY (parent) REFERENCES tasks_category(id);


--
-- Name: task_tags-question_tag_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "tasks_tags-task"
ADD CONSTRAINT "task_tags-question_tag_fkey" FOREIGN KEY (tag) REFERENCES data_tags(id);


--
-- Name: task_tags-question_task_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "tasks_tags-task"
ADD CONSTRAINT "task_tags-question_task_fkey" FOREIGN KEY (task) REFERENCES tasks_task(id);


--
-- Name: tasks_category-task_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "tasks_category-task"
ADD CONSTRAINT "tasks_category-task_category_fkey" FOREIGN KEY (category) REFERENCES tasks_category(id);


--
-- Name: tasks_category-task_task_fkey; Type: FK CONSTRAINT; Schema: public; Owner: animatrz
--

ALTER TABLE ONLY "tasks_category-task"
ADD CONSTRAINT "tasks_category-task_task_fkey" FOREIGN KEY (task) REFERENCES tasks_task(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: animatrz
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM animatrz;
GRANT ALL ON SCHEMA public TO animatrz;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

