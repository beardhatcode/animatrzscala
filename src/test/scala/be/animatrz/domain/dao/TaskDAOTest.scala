package be.animatrz.domain.dao

import org.scalatest._
import be.animatrz.domain.data.Task
/**
 * Created by garonn on 16/06/15.
 */
class TaskDAOTest extends FlatSpec with Matchers with ResetDBToTest{
  val dao = TaskDAO()

  behavior of "getQuestionbyId"
  it should "get the right data" in {
    val q1 = dao.getTaskById(1).get
    q1.id should be(1)
    q1.what should be("Doe een dansje")
    q1.more should be("een mooi dansje")
  }

  it should "return the same instance" in {
    val q1 = dao.getTaskById(1).get
    val q2 = dao.getTaskById(1).get
    q1 should be theSameInstanceAs  q2
  }

  it should "return None if no result" in {
    dao.getTaskById(Integer.MAX_VALUE)should be(None)
  }

  behavior of "Categories"
  it should "correctness" in {
    val q1 = dao.getTaskById(1).get
    val categories = q1.categories
    categories.foreach( cat => {
      println(s"Testing $cat")
      cat.data.toSet should contain(q1)
    })
    categories should have size(2)
  }

  it should "reflect changes in domain and DB" in {
    val dbPrepared = getConnection().prepareStatement("SELECT * FROM \"tasks_category-task\" WHERE category=7 AND task=2")

    val q2 = dao.getTaskById(2).get
    val category = CategoryDAO.forTask.byShortName("TEST").get

    category.data ++ q2
    dbPrepared.executeQuery().next should be(true)
    category.data.toSet should contain(q2)
    category.data -- q2
    dbPrepared.executeQuery().next should be(false)

    q2.categories ++ category
    dbPrepared.executeQuery().next should be(true)
    category.data.toSet should contain(q2)

    q2.categories -- category
    dbPrepared.executeQuery().next should be(false)

  }

  behavior of "Tags"
  it should "have the correct tags" in {
    val q1 = dao.getTaskById(1).get
    val tags1 = q1.tags
    tags1.foreach(cat => {
      println(s"Testing $cat")
      cat.data.toSet should contain(q1)
    })

    tags1 should have size(2)

    TagDAO().getByName("Schoonheid").get.getData.foreach({
      case Task(id,what,more,_,_) => println(what + "=> " + more)
      case _ => println("hmm")})
  }

  it should "reflect changes in domain and DB" in {
    val dbPrepared = getConnection().prepareStatement("SELECT * FROM \"tasks_tags-task\" WHERE tag=3 AND task=2")

    val q2 = dao.getTaskById(2).get
    val tag = TagDAO().getByName("TESTTAG").get
    tag.data ++ q2
    dbPrepared.executeQuery().next should be(true)
    tag.data -- q2
    dbPrepared.executeQuery().next should be(false)
    tag.data.toSet should have size(0)
    q2.tags ++ tag
    dbPrepared.executeQuery().next should be(true)
    q2.tags -- tag
    dbPrepared.executeQuery().next should be(false)
    tag.data.toSet should have size(0)
  }

}