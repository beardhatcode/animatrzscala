package be.animatrz.domain.dao

import be.animatrz.domain.marker.Tag
import org.scalatest._

/**
 * Created by garonn on 28/06/15.
 */
class TagDAOTest extends FlatSpec with Matchers with ResetDBToTest{
  behavior of "TagDAO"

  it should "Get tags correctly" in {
    TagDAO().getByName("TESTTAG").get should matchPattern {case Tag("TESTTAG") => }
    TagDAO().getByName("IK BESTA NIET") should be(None)
  }

}
