package be.animatrz.domain.dao

/**
 * Created by garonn on 28/06/15.
 */
trait ResetDBToTest {
  println(ExecuteShellComand.executeCommand("psql -U animatrz -f src/test/resources/ddl.sql"))
}
