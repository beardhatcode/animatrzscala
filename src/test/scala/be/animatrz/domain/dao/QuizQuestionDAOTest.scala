package be.animatrz.domain.dao

import be.animatrz.domain.data.QuizQuestion
import org.scalatest.{Matchers, FlatSpec}


/**
 * Created by garonn on 16/06/15.
 */
class QuizQuestionDAOTest extends FlatSpec with Matchers with ResetDBToTest{
  val dao = QuizQuestionDAO()

  behavior of "getQuestionbyId"
  it should "get the right data" in {
    val q1 = dao.getQuestionById(1).get
    q1.id should be(1)
    q1.question should be("Wie is het mooiste meisje ooit?")
    q1.answer should be("Lieselotte")
  }

  it should "return the same instance" in {
    val q1 = dao.getQuestionById(1).get
    val q2 = dao.getQuestionById(1).get
    q1 should be theSameInstanceAs  q2
  }

  it should "return None if no result" in {
    dao.getQuestionById(Integer.MAX_VALUE)should be(None)
  }

  behavior of "Categories"
  it should "correctness" in {
    val q1 = dao.getQuestionById(1).get
    val categories = q1.categories
    categories.foreach( cat => {
      println(s"Testing $cat")
      cat.data.toSet should contain(q1)
    })
    categories should have size(2)
  }

  it should "reflect changes in domain and DB" in {
    val dbPrepared = getConnection().prepareStatement("SELECT * FROM \"quiz_questions_category-question\" WHERE category=4 AND question=2")

    val q2 = dao.getQuestionById(2).get
    val category = CategoryDAO.forQuizQuestion.byShortName("ja-nee").get

    category.data ++ q2
    dbPrepared.executeQuery().next should be(true)
    category.data.toSet should contain(q2)
    category.data -- q2
    dbPrepared.executeQuery().next should be(false)

    q2.categories ++ category
    dbPrepared.executeQuery().next should be(true)
    category.data.toSet should contain(q2)

    q2.categories -- category
    dbPrepared.executeQuery().next should be(false)

  }

  behavior of "Tags"
  it should "have the correct tags" in {
    val q1 = dao.getQuestionById(1).get
    val tags1 = q1.tags
    tags1.foreach(cat => {
      println(s"Testing $cat")
      cat.data.toSet should contain(q1)
    })

    tags1 should have size(2)

    TagDAO().getByName("Schoonheid").get.getData.foreach({
      case QuizQuestion(id,question,answer,_,_,_) => println(question + "=> " + answer)
      case _ => println("hmm")})
  }

  it should "reflect changes in domain and DB" in {
    val dbPrepared = getConnection().prepareStatement("SELECT * FROM \"quiz_questions_tags-question\" WHERE tag=3 AND question=2")

    val q2 = dao.getQuestionById(2).get
    val tag = TagDAO().getByName("TESTTAG").get
    tag.data ++ q2
    dbPrepared.executeQuery().next should be(true)
    tag.data -- q2
    dbPrepared.executeQuery().next should be(false)
    tag.data.toSet should have size(0)
    q2.tags ++ tag
    dbPrepared.executeQuery().next should be(true)
    q2.tags -- tag
    dbPrepared.executeQuery().next should be(false)
    tag.data.toSet should have size(0)
  }

}