package be.animatrz.domain.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

/**
 * This class is vomit, I mean an utility to get resources from the resource folder
 */
public class ResourceLoader {
    public static File getFile(String filename) throws FileNotFoundException{
        URL resource = ResourceLoader.class.getResource("../../../../"+filename);
        try {
            return new File(resource.toURI());
        } catch (Exception e) {
            throw new FileNotFoundException("I couln't fucking load ../../../../"+filename +" relative to ResourceLoader.class");
        }
    }

    public static InputStream getFileStream(String filename) throws FileNotFoundException {
        return new FileInputStream(getFile(filename));
    }
}
