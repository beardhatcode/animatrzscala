package be.animatrz.domain.util

/**
 * Created by garonn on 12/06/15.
 */
trait Identifiable {
  def id: Int
  def kind :String
}
