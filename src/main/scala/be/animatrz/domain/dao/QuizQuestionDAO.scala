package be.animatrz.domain.dao

import java.sql.{Connection, ResultSet}

import be.animatrz.domain.data.{QuizQuestion,Data}
import be.animatrz.domain.marker.{Tag, QuizQuestionCategory, Category}
import be.animatrz.domain.util.{MultiRemovalEvent, MultiAdditionEvent, Multi}

import scala.collection.mutable

/**
 * DAO for QuizQuestions.
 * Note: If 2 elements that are returened are the same, they must be the same instance
 * @param conn a function that gets a working connection
 */
class QuizQuestionDAO(val conn : ()=>Connection) extends FetchingDAO[QuizQuestion]{


  protected val tableAliased = "quiz_questions_question AS qq"
  protected val fields = "qq.id,qq.question,qq.answer,qq.extra"

  private val cache = new mutable.WeakHashMap[Int,QuizQuestion]

  /**
   * Make a QuizQuesion from the current state of a resultset and bind listerners to to the tags and categories
   * @param rs the result set to look att
   * @return
   */
  protected def build(rs: ResultSet): QuizQuestion = {
    val id:Int =  rs.getInt("id")

    val contained = cache.synchronized(cache.contains(id))

    if(contained) {
      return cache.get(id).get
    }
    lazy val ret: QuizQuestion = new QuizQuestion(
      id,
      rs.getString("question"), rs.getString("answer"), rs.getString("extra"),
      categories = Multi.makeLazy[QuizQuestion,Category[QuizQuestion]](()=>ret,()=>this.getCategory(id),_.data),
      tags = Multi.makeLazy[Data[_],Tag](()=>ret,()=>this.getTag(id),_.data)
    )

    ret.categories.addListener{
      case MultiAdditionEvent(_,element:QuizQuestionCategory) => CategoryDAO.forQuizQuestion.addData(cat=element,el=ret)
      case MultiRemovalEvent(_,element:QuizQuestionCategory) => CategoryDAO.forQuizQuestion.removeData(cat=element,el=ret)
    }
    ret.tags.addListener{
      case MultiAdditionEvent(_,element:Tag) => TagDAO().addData(tag=element,el=ret)
      case MultiRemovalEvent(_,element:Tag) => TagDAO().removeData(tag=element,el=ret)
    }

    cache.synchronized(cache.getOrElseUpdate(id,ret))
  }

  /**
   * Get question by id
   * @param id the id of the question we want
   * @return
   */
  def getQuestionById(id: Int):Option[QuizQuestion] = fetchSingle(" WHERE id = ? LIMIT 1",_.setInt(1,id))

  /**
   * Get the multi's of categories to initialise the Multi
   * @param id the id of the question
   * @return a mutable set of categorie multi's pointing to the given question
   */
  private def getCategory(id: Int): mutable.Set[Multi[Category[QuizQuestion], QuizQuestion]] = {
    val data = CategoryDAO.forQuizQuestion.fetchData(
      " LEFT JOIN \"quiz_questions_category-question\" as rel on rel.category = c.id  WHERE rel.question = ?",
       _.setInt(1, id)
    ).map(_.data)

    mutable.Set(data.toSeq:_*)
  }

  /**
   * Get the multi's of tag to initialise the Multi
   * @param id the id of the question
   * @return a mutable set of tag multi's pointing to the given question
   */
  private def getTag(id: Int) = {
    val data = TagDAO().fetchData(
      " LEFT JOIN \"quiz_questions_tags-question\" as rel on rel.tag = dt.id  WHERE rel.question = ?",
      _.setInt(1, id)
    ).map(_.data)
    mutable.Set(data.toSeq:_*)
  }
}


object QuizQuestionDAO{
  private val dao =  new QuizQuestionDAO(getConnection)
  def apply() = dao
}
