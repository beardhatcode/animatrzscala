package be.animatrz.domain

import java.sql.DriverManager
import java.util.Properties

import be.animatrz.domain.util.ResourceLoader

/**
 * Package object that supplies the DAO's with a connection as specified in
 * the config.properties
 * @todo when should the connection be closed?
 * @todo sense that a connection has been closed
 */
package object dao {
  private val props = new Properties();
  props.load(ResourceLoader.getFileStream("config.properties"))
  private val username = props.getProperty("db.username")
  private val password = props.getProperty("db.password")
  private val database = props.getProperty("db.database")
  Class.forName(props.getProperty("db.driver")).newInstance()

  private val getConnectionFun = if(username == null) {
    ()=>{DriverManager.getConnection(database)}
  }
  else {
    ()=>{DriverManager.getConnection(database, username, password)}
  }

  var connection = getConnectionFun()
  def getConnection = ()=>{connection}
}
