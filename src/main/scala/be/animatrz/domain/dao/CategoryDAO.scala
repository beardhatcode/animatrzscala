package be.animatrz.domain.dao

import java.sql._

import be.animatrz.domain.data.{QuizQuestion, Data}
import be.animatrz.domain.marker.{Category}

/**
 * Abstract Category DAO to get information about categories from the database.
 * @param conn  a function that return a alive connection
 * @param table The table in th db containing category info
 * @tparam T  The type off the data contaned in the category
 */
abstract class CategoryDAO[T <: Data[T]](val conn : ()=>Connection, val table:String,val joinTable:(String,String,String)) extends FetchingDAO[Category[T]]{

  val fields = "c.id c_id,c.parent c_parent,c.name c_name,c.short_name c_short_name,c.icon c_icon,c.description c_description"
  val tableAliased = table + " AS c"

  /**
   * Get the category from the DB for a certain short name
   * @param name  short name of the class
   * @return the category that matches
   */
  def byShortName(name: String):Option[Category[T]] = {
    this.fetchSingle(" WHERE short_name = ? LIMIT 1",_.setString(1,name))
  }

  /**
   * Link a category and an element
   * @param cat The category
   * @param el  The element to add to the category
   */
  def addData(cat: Category[T], el: T): Unit = {
    val stmt = conn().prepareStatement(
      "INSERT INTO " +joinTable._1+ " (\"" +joinTable._2+ "\",\""+joinTable._3+"\") VALUES (?, ?)"
    )
    stmt.setInt(1,cat.id)
    stmt.setInt(2,el.id)
    stmt.execute()
  }

  /**
   * Unlink an element from a category
   * @param cat
   * @param el
   */
  def removeData(cat: Category[T], el: T): Unit = {
    val stmt = conn().prepareStatement(
      "DELETE FROM " +joinTable._1+ "  WHERE  " +joinTable._2+ "=? AND "+joinTable._3+"=?"
    )
    stmt.setInt(1,cat.id);
    stmt.setInt(2,el.id);
    stmt.execute()
  }
}


/**
 * Object to use as injector, provides singleton like instances of category DAOs
 */
object CategoryDAO{
  val daoTask = new TaskCategoryDAO(getConnection)
  def forTask = daoTask

  val daoQuizQuestion = new QuizCategoryDAO(getConnection)
  def forQuizQuestion =daoQuizQuestion
}


