package be.animatrz.domain.dao

import java.sql.{Connection, PreparedStatement}

/**
 * Traits for DAO's that fetch data from a single table and have a straightforward build
 * function.
 * @tparam T  Type of the thing that should be fetched
 */
trait FetchingDAO[T] extends BuildingDAO[T]{
  def conn:()=>Connection

  /**
   * @return A CSV string that contains the columns needed to find the result
   */
  protected def fields: String

  /**
   * @return The name of the table in the form "tablename AS tablealias"
   */
  protected def tableAliased: String

  /**
   * Get data for the type by specifying only joins and where (and limit, ...)
   * @param queryLogic    The string to be placed after SELECT ... FROM ...
   * @param stmtBinding   Method called for binding params
   * @return  The result of the query
   */
  def fetchData(queryLogic:String , stmtBinding: (PreparedStatement) => Any):Seq[T] = {
    val connection = conn()
    val stmt = connection.prepareStatement(
      "SELECT "+fields+" FROM "+tableAliased  + " " + queryLogic
    )
    println("SELECT "+fields+" FROM "+tableAliased  + " " + queryLogic)
    stmtBinding(stmt)
    val rs = stmt.executeQuery()
    val result = buildAll(rs)
    rs.close()
    result
  }

  /**
   * Get data for the type by specifying only joins and where (and limit, ...) Will
   * return only one result in option
   * @param queryLogic    The string to be placed after SELECT ... FROM ...
   * @param stmtBinding   Method called for binding params
   * @return  The result of the query
   */
  def fetchSingle(queryLogic: String, stmtBinding: (PreparedStatement) => Any): Option[T] = {
    val connection = conn()
    val stmt = connection.prepareStatement(
      "SELECT " + fields + " FROM " + tableAliased + " " + queryLogic
    )
    stmtBinding(stmt)
    val rs = stmt.executeQuery()
    var result:Option[T] = None
    if(rs.next()) {
      result = Option(build(rs))
    }
    rs.close()
    result
  }
}
