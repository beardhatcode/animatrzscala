package be.animatrz.domain.dao

import java.sql.{Connection, ResultSet}

import be.animatrz.domain.data.{Task,Data}
import be.animatrz.domain.marker.{TaskCategory, Tag, Category}
import be.animatrz.domain.util.{MultiRemovalEvent, MultiAdditionEvent, Multi}

import scala.collection.mutable

/**
 * DAO for QuizQuestions.
 * Note: If 2 elements that are returened are the same, they must be the same instance
 * @param conn a function that gets a working connection
 */
class TaskDAO(val conn : ()=>Connection) extends FetchingDAO[Task]{


  protected val tableAliased = "tasks_task AS tt"
  protected val fields = "tt.id,tt.what,tt.extra"

  private val cache = new mutable.WeakHashMap[Int,Task]

  /**
   * Make a QuizQuesion from the current state of a resultset and bind listerners to to the tags and categories
   * @param rs the result set to look att
   * @return
   */
  protected def build(rs: ResultSet): Task = {
    val id:Int =  rs.getInt("id")

    val contained = cache.synchronized(cache.contains(id))

    if(contained) {
      return cache.get(id).get
    }
    lazy val ret: Task = new Task(
      id,
      rs.getString("what"), rs.getString("extra"),
      categories = Multi.makeLazy[Task,Category[Task]](()=>ret,()=>this.getCategory(id),_.data),
      tags = Multi.makeLazy[Data[_],Tag](()=>ret,()=>this.getTag(id),_.data)
    )

    ret.categories.addListener{
      case MultiAdditionEvent(_,element:TaskCategory) => CategoryDAO.forTask.addData(cat=element,el=ret)
      case MultiRemovalEvent(_,element:TaskCategory) => CategoryDAO.forTask.removeData(cat=element,el=ret)
    }
    ret.tags.addListener{
      case MultiAdditionEvent(_,element:Tag) => TagDAO().addData(tag=element,el=ret)
      case MultiRemovalEvent(_,element:Tag) => TagDAO().removeData(tag=element,el=ret)
    }

    cache.synchronized(cache.getOrElseUpdate(id,ret))
  }

  /**
   * Get question by id
   * @param id the id of the question we want
   * @return
   */
  def getTaskById(id: Int):Option[Task] = fetchSingle(" WHERE id = ? LIMIT 1",_.setInt(1,id))

  /**
   * Get the multi's of categories to initialise the Multi
   * @param id the id of the question
   * @return a mutable set of categorie multi's pointing to the given question
   */
  private def getCategory(id: Int): mutable.Set[Multi[Category[Task], Task]] = {
    val data = CategoryDAO.forTask.fetchData(
      " LEFT JOIN \"tasks_category-task\" as rel on rel.category = c.id  WHERE rel.task = ?",
      _.setInt(1, id)
    ).map(_.data)

    mutable.Set(data.toSeq:_*)
  }

  /**
   * Get the multi's of tag to initialise the Multi
   * @param id the id of the question
   * @return a mutable set of tag multi's pointing to the given question
   */
  private def getTag(id: Int) = {
    val data = TagDAO().fetchData(
      " LEFT JOIN \"tasks_tags-task\" as rel on rel.tag = dt.id  WHERE rel.task = ?",
      _.setInt(1, id)
    ).map(_.data)
    mutable.Set(data.toSeq:_*)
  }
}


object TaskDAO{
  private val dao =  new TaskDAO(getConnection)
  def apply() = dao
}
