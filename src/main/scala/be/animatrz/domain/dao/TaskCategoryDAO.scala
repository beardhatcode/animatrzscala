package be.animatrz.domain.dao

import java.sql.{ResultSet, Connection}

import be.animatrz.domain.data.Task
import be.animatrz.domain.marker.{Category, TaskCategory,Tag}
import be.animatrz.domain.util.{MultiRemovalEvent, MultiAdditionEvent, Multi}

import scala.collection.mutable

/**
 * DAO for Categories for quizes
 * @param conn  a function that return a alive connection
 */
class TaskCategoryDAO(conn: () => Connection) extends CategoryDAO[Task](
  conn,
  table = "tasks_category",
  joinTable = ("\"tasks_category-task\"", "category", "task")
) {


  private val cache = new mutable.WeakHashMap[Int,Category[Task]]
  /**
   * Make a QuizQuesion from the current state of a resultset
   * @param rs the result set to look att
   * @return
   */
  protected def build(rs: ResultSet): Category[Task] = {
    val id:Int =  rs.getInt("c_id")
    val shortName = rs.getString("c_short_name")

    if(cache.synchronized(cache.contains(id))) {
      return cache.get(id).get
    }
    lazy val ret:TaskCategory = new TaskCategory(id, rs.getString("c_name"),
      shortName, rs.getString("c_description"),
      Multi.makeLazy[Category[Task],Task](()=>ret,()=>this.getData(id),_.categories)
    )
    ret.data.addListener{
      case MultiAdditionEvent(_,element:Task) => this.addData(cat=ret,el=element)
      case MultiRemovalEvent(_,element:Task) => this.removeData(cat=ret,el=element)
    }
    //Only update if still not in set
    cache.synchronized(cache.getOrElseUpdate(id,ret))
  }

  /**
   * Get the multi's of tag to initialise the Multi
   * @param id the id of the category
   * @return a mutable set of tag multi's pointing to the given question
   */
  private def getData(id: Int) = {
    val data = TaskDAO().fetchData(
      "LEFT JOIN \"tasks_category-task\" as rel on tt.id = rel.task " +
        "WHERE rel.category = ?",
      _.setInt(1, id)
    ).map(_.categories)

    mutable.Set(data.toSeq:_*)
  }
}
