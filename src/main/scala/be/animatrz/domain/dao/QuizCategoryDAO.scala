package be.animatrz.domain.dao

import java.sql.{ResultSet, Connection}

import be.animatrz.domain.data.QuizQuestion
import be.animatrz.domain.marker.{Category, QuizQuestionCategory,Tag}
import be.animatrz.domain.util.{MultiRemovalEvent, MultiAdditionEvent, Multi}

import scala.collection.mutable

/**
 * DAO for Categories for quizes
 * @param conn  a function that return a alive connection
 */
class QuizCategoryDAO(conn: () => Connection) extends CategoryDAO[QuizQuestion](
  conn,
  table = "quiz_questions_category",
  joinTable = ("\"quiz_questions_category-question\"", "category", "question")
  ) {


  private val cache = new mutable.WeakHashMap[Int,Category[QuizQuestion]]
  /**
   * Make a QuizQuesion from the current state of a resultset
   * @param rs the result set to look att
   * @return a quizquesiton
   */
  protected def build(rs: ResultSet): Category[QuizQuestion] = {
    val id:Int =  rs.getInt("c_id")
    val shortName = rs.getString("c_short_name")

    if(cache.synchronized(cache.contains(id))) {
      return cache.get(id).get
    }
    lazy val ret:QuizQuestionCategory = new QuizQuestionCategory(id, rs.getString("c_name"),
      shortName, rs.getString("c_description"),
      Multi.makeLazy[Category[QuizQuestion],QuizQuestion](()=>ret,()=>this.getData(id),_.categories)
    )
    ret.data.addListener{
      case MultiAdditionEvent(_,element:QuizQuestion) => this.addData(cat=ret,el=element)
      case MultiRemovalEvent(_,element:QuizQuestion) => this.removeData(cat=ret,el=element)
    }
    //Only update if still not in set
    cache.synchronized(cache.getOrElseUpdate(id,ret))
  }

  /**
   * Get the multi's of tag to initialise the Multi
   * @param id the id of the category
   * @return a mutable set of tag multi's pointing to the given question
   */
  private def getData(id: Int) = {
      val data = QuizQuestionDAO().fetchData(
        "LEFT JOIN \"quiz_questions_category-question\" as rel on qq.id = rel.question " +
          "WHERE rel.category = ?",
        _.setInt(1, id)
      ).map(_.categories)

      mutable.Set(data.toSeq:_*)
    }
}
