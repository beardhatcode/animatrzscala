package be.animatrz.domain.dao

import java.sql.ResultSet

import be.animatrz.domain.marker.Category

/**
 * Trait for a DAO that has a build method that builds from a Result set
 * @tparam T  Type of the thing being built
 */
trait BuildingDAO[T]{
  /**
   * Build a class from a row in the DB, availible parameters are in `fields`
   * @param rs  Result Set of the Select query
   * @return A category that is identifiable
   */
  protected def build(rs: ResultSet): T

  /**
   * Build all result from a result set
   * @param rs  Result Set of the Select query
   * @return A list of T elements thatw ere fetched, empty list if no result
   */
  protected def buildAll(rs: ResultSet): List[T] ={
    Iterator.continually((rs.next(), rs)).takeWhile(_._1).map(r => build(r._2)).toList
  }
}
