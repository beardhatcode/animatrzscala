package be.animatrz.domain.dao

import java.sql.{ResultSet, Connection}

import be.animatrz.domain.data.{Task, QuizQuestion, Data}
import be.animatrz.domain.marker.{QuizQuestionCategory, TagImpl, Category, Tag}
import be.animatrz.domain.util.{MultiRemovalEvent, MultiAdditionEvent, Multi}

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
 * Created by garonn on 15/06/15.
 */

class TagDAO (val conn : ()=>Connection) extends FetchingDAO[Tag]{

  override def tableAliased: String = "data_tags AS dt"

  override def fields: String = "dt.id AS dt_id, dt.name AS dt_name"


  def getByName(name:String):Option[Tag] = fetchSingle("WHERE dt.name = ?",_.setString(1,name))

  val cache = new mutable.WeakHashMap[Int,Tag]
  /**
   * Build a class from a row in the DB, availible parameters are in `fields`
   * @param rs  Result Set for the Select query
   * @return A category that is identifiable
   */
  override protected def build(rs: ResultSet): Tag = {
    val id:Int =  rs.getInt("dt_id")

    val contained = cache.synchronized(cache.contains(id))

    if(contained) {
      return cache.get(id).get
    }
    lazy val ret: Tag = new TagImpl(id,rs.getString("dt_name"),
      data = Multi.makeLazy[Tag,Data[_]](()=>ret,()=>this.getDataMulti(id),_.tags)
    )

    ret.data.addListener{
      case MultiAdditionEvent(_,element:Data[_]) => this.addData(tag=ret,el=element)
      case MultiRemovalEvent(_,element:Data[_]) => this.removeData(tag=ret,el=element)
    }

    cache.synchronized(cache.getOrElseUpdate(id,ret))
  }

  private def getJoinTable(el: Data[_]): Try[(String, String, String)] =  el match {
    case QuizQuestion(_,_,_,_,_,_) => Success(("\"quiz_questions_tags-question\"", "tag", "question"))
    case Task(_,_,_,_,_) => Success(("\"tasks_tags-task\"", "tag", "task"))
      case _ =>  Failure(new IllegalArgumentException("Type of data not recognised"))
  }

  def addData(tag: Tag, el: Data[_]): Unit = {
    val joinTable = getJoinTable(el).get
    val connection = conn();
    val stmt = connection.prepareStatement(
      "INSERT INTO " +joinTable._1+ " (\"" +joinTable._2+ "\",\""+joinTable._3+"\") VALUES (?, ?)"
    )
    stmt.setInt(1,tag.id);
    stmt.setInt(2,el.id);
    stmt.execute()
  }

  def removeData(tag: Tag, el: Data[_]): Unit = {
    val joinTable = getJoinTable(el).get
    val connection = conn();
    val stmt = connection.prepareStatement(
      "DELETE FROM " +joinTable._1+ "  WHERE  " +joinTable._2+ "=? AND "+joinTable._3+"=?"
    )
    stmt.setInt(1,tag.id);
    stmt.setInt(2,el.id);
    stmt.execute()
  }

  def getDataMulti(id: Int): mutable.Set[Multi[Data[_], Tag]] = {
    val ret =  mutable.Set.empty[Multi[Data[_], Tag]]
    QuizQuestionDAO().fetchData(
      " LEFT JOIN \"quiz_questions_tags-question\" as rel on rel.question = qq.id  WHERE rel.tag = ?",
      stmt => {stmt.setInt(1,id)}
    ).map(_.tags).foreach(ret add)
    TaskDAO().fetchData(
      " LEFT JOIN \"tasks_tags-task\" as rel on rel.task = tt.id  WHERE rel.tag = ?",
      stmt => {stmt.setInt(1,id)}
    ).map(_.tags).foreach(ret add)

    ret
  }
}

object TagDAO {
  private val instance = new TagDAO(getConnection)
  def apply() = instance
}
