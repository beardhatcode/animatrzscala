package be.animatrz.domain.marker

import be.animatrz.domain.dao.{TaskDAO, CategoryDAO}
import be.animatrz.domain.data.{Data, Task, QuizQuestion}
import be.animatrz.domain.util.{Multi, Identifiable}

/**
 * Abstract class represents
 * @param name        Name of the category
 * @param shortName   A sort name of the category
 * @param description A description
 * @tparam T          Type of the content
 */
abstract case class Category[T <: Data[T]](var name :String, var shortName :String, var description : String) extends Identifiable{
  val data:Multi[Category[T],T]
  def getData :Set[T]
}

class QuizQuestionCategory(val id: Int,  name:String , shortName :String,  description : String,val data: Multi[Category[QuizQuestion], QuizQuestion])
  extends Category[QuizQuestion] (name,shortName, description) {
  def kind = "QuizQuestionCategory"

  override def getData: Set[QuizQuestion] = data.getOthers()
}

class TaskCategory(val id: Int,  name:String , shortName :String,  description : String,val data: Multi[Category[Task], Task])
  extends Category[Task] (name,shortName, description) {
  def kind = "TaksCategory"

  override def getData: Set[Task] = data.getOthers()
}

object Category{
  def getByShortNameQuizQuestion(shortName :String):Category[QuizQuestion] =
    CategoryDAO.forQuizQuestion.byShortName(shortName).get
}