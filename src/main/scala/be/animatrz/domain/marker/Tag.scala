package be.animatrz.domain.marker

import be.animatrz.domain.dao.TagDAO
import be.animatrz.domain.data.Data
import be.animatrz.domain.util.{Multi, Identifiable}

/**
 * Created by garonn on 10/06/15.
 */
abstract case class Tag(name:String) extends Identifiable{
  val data:Multi[Tag,Data[_]]
  def getData:Set[Data[_]] = data.getOthers()
}

class TagImpl(val id:Int, name:String, val data: Multi[Tag, Data[_]]) extends  Tag(name) {
  override def kind: String = "tag"
}

object Tag{

}
