package be.animatrz.domain.data

import be.animatrz.domain.marker.{QuizQuestionCategory, Tag, Category}
import be.animatrz.domain.util.{Multi, Identifiable}

/**
 * A quiz question
 * @param id
 * @param question
 * @param answer
 * @param more
 * @param categories
 * @param tags
 */
case class QuizQuestion(id:Int, var question: String,var answer: String, var more: String,
                        categories: Multi[QuizQuestion,Category[QuizQuestion]], tags: Multi[Data[_],Tag])
  extends Data[QuizQuestion] {
  override def kind: String = "QuizQuestion"

}

