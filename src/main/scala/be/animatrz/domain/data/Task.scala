package be.animatrz.domain.data

import be.animatrz.domain.marker.{Tag, Category}
import be.animatrz.domain.util.{Multi, Identifiable}

/**
 * Created by garonn on 11/06/15.
 */
case class Task(id:Int,what:String, more:String,
                categories: Multi[Task,Category[Task]], tags: Multi[Data[_],Tag])
  extends Data[Task] {
  override def kind: String = "Task"
}