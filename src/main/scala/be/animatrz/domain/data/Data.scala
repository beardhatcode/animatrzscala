package be.animatrz.domain.data

import be.animatrz.domain.marker.{Tag, Category}
import be.animatrz.domain.util.{Multi, Identifiable}

/**
 * The abstract data class; parent off all data classes
 * @tparam T The class it is parent of
 */
abstract class Data[T <: Data[T]] extends  Identifiable{
  /**
   * @return A multi that links to the categories of this element
   */
  def categories :Multi[T,Category[T]]

  /**
   * @return A multi that links to all the tags of this element
   */
  def tags :Multi[Data[_],Tag]


  def getCategories: Set[Category[T]] = categories.getOthers[Category[T]]
  def getTags: Set[Tag] = tags.getOthers[Tag]
}
